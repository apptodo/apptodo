package com.makowska.dto;

import javax.validation.constraints.NotBlank;

public class FormRegister {

    @NotBlank(message = "wpisz login")
    private String username;

    @NotBlank(message = "wpisz hasło")
    private String password;

    @NotBlank(message = "wpisz imie")
    private String firstName;

    @NotBlank(message = "wpisz nazwisko")
    private String lastName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
