package com.makowska.service;

import com.makowska.dto.FormRegister;
import com.makowska.enums.RoleEnum;
import com.makowska.model.User;
import com.makowska.repository.RoleRepository;
import com.makowska.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserService {

    private UsersRepository usersRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UsersRepository usersRepository, RoleRepository roleRepository) {
        this.usersRepository = usersRepository;
        this.roleRepository = roleRepository;
    }


    public User addUser(FormRegister formRegister){

        User user = new User();
        user.setUsername(formRegister.getUsername());
        user.setPassword(passwordEncoder.encode(formRegister.getPassword()));
        user.setFirstName(formRegister.getFirstName());
        user.setLastName(formRegister.getLastName());
        user.setRole(roleRepository.findByName(RoleEnum.ROLE_USER.getRoleName()));

        User newUser = usersRepository.save(user);
        return newUser;
    }

}
