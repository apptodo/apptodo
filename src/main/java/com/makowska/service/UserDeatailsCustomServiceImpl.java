package com.makowska.service;

import com.makowska.model.User;
import com.makowska.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDeatailsCustomServiceImpl implements UserDetailsService {

    private UsersRepository usersRepository;

    @Autowired
    public UserDeatailsCustomServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    //to jest ten moment kiedy sie logujemy juz po kliknieciu login
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = usersRepository.findByUserName(username);
        List authorieties = new ArrayList();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(user.getRole().getName());
        authorieties.add(simpleGrantedAuthority);
        //teraz tworzymy obiekt springowy, usera springowego
        org.springframework.security.core.userdetails.User springUser =
                new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorieties);
        return springUser;
    }
}
