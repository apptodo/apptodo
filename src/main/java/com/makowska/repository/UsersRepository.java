package com.makowska.repository;

import com.makowska.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository <User, Long> {

    User findByUserName (String username);
}
