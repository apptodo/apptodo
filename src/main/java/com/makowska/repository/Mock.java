package com.makowska.repository;

import com.makowska.enums.RoleEnum;
import com.makowska.model.Role;

import com.makowska.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Mock {

    private RoleRepository roleRepository;
    private UsersRepository usersRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public Mock (RoleRepository roleRepository, UsersRepository usersRepository){
        this.roleRepository = roleRepository;
        this.usersRepository = usersRepository;
    }

    //utworzyć dwie rolre - ROLE_USER i ROLE_ADMIN oraz użytkownika admin i user

    @PostConstruct
    public void mockData(){
        Role roleAdmin = roleRepository.findByName(RoleEnum.ROLE_ADMIN.getRoleName());
        if (roleAdmin == null){
            roleAdmin = new Role();
            roleAdmin.setName(RoleEnum.ROLE_ADMIN.getRoleName());
            roleRepository.save(roleAdmin);
        }

        Role roleUser = roleRepository.findByName(RoleEnum.ROLE_USER.getRoleName());
        if (roleUser == null){
            roleUser = new Role();
            roleUser.setName(RoleEnum.ROLE_USER.getRoleName());
            roleRepository.save(roleUser);
        }

        User admin = usersRepository.findByUserName("admin");
        if (admin == null){
            admin = new User();
            admin.setUsername("admin");
            admin.setFirstName("Jan");
            admin.setLastName("Kowalski");
            admin.setPassword(passwordEncoder.encode("admin1234")); //zahaszujemy
            admin.setRole(roleAdmin);
            usersRepository.save(admin);
        }

        User user = usersRepository.findByUserName("user");
        if (user == null){
            user = new User();
            user.setUsername("admin");
            user.setFirstName("Jan");
            user.setLastName("Kowalski");
            user.setPassword(passwordEncoder.encode("user1234")); //zahaszujemy
            user.setRole(roleUser);
            usersRepository.save(user);

        }

    }
}
