package com.makowska.enums;

import lombok.Getter;

@Getter
public enum RoleEnum {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_USER("ROLE_USER");

    private String roleName;
    //private String priority;

    RoleEnum(String roleName) {
        this.roleName = roleName;
    }

//    RoleEnum(String roleName, String priority) {
//        this.roleName = roleName;
//        this.priority = priority;
//    }

    public String getRoleName() {
        return roleName;
    }
}
