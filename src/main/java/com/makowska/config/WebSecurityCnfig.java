package com.makowska.config;

import com.makowska.service.UserDeatailsCustomServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration
@EnableWebSecurity
public class WebSecurityCnfig extends WebSecurityConfigurerAdapter {

    private UserDeatailsCustomServiceImpl userDeatailsCustomService;

    @Autowired
    public WebSecurityCnfig(UserDeatailsCustomServiceImpl userDeatailsCustomService) {
        this.userDeatailsCustomService = userDeatailsCustomService;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests()
                .antMatchers("/").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                //mogą wejść osoby które chcą się zarejestrować
                .antMatchers("/register").permitAll()
                //dzięki temu możemy wejśc na h2
                .antMatchers("/h2/**").permitAll()
                //zabezpieczenie CSRF, aby go wyłączyć piszemy
                .and()
                .csrf().disable()
                //chroni przed wstrzykiwaniem css, wyłączamy
                .headers().frameOptions().disable()
                //jeśli uda nam się wywolas to przekeiruj na "/"
                //tzn ze bedzie formularz do zalogowania dostarczony przez spring:
                .and().formLogin().loginPage("/login").defaultSuccessUrl("/").permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDeatailsCustomService);
    }
}
