package com.makowska.controller;

import com.makowska.dto.FormRegister;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @GetMapping
    public String showRegister(Model model) {
        model.addAttribute("formReg", new FormRegister());
        return "formRegister";
    }

    @PostMapping
    public String showFormRegister(
            @Valid @ModelAttribute("formReg") FormRegister formRegister,
            BindingResult bindingResult,
            Model model) {

        if (bindingResult.hasErrors()) {
            return "formRegister";
        } else {
            model.addAttribute("inputValue", formRegister.getUsername());
            model.addAttribute("inputValue", formRegister.getPassword());
            model.addAttribute("inputValue", formRegister.getFirstName());
            model.addAttribute("inputValue", formRegister.getLastName());
            return "formRegister";
        }
    }
}
