package com.makowska.projektToDo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektToDoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektToDoApplication.class, args);
	}

}
